tailwind.config = {
  theme: {
    extend: {
      colors: {
        "light-gray": "#F4F2F3",
        "dark-gray": "#5B5452",
        "dark-green": "#08898C",
        "light-green": "#00D7BE",
      },
    },
  },
};
